package workshop.chapter11.v1;

import workshop.util.CommonUtils;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public record Shop(String name) {

    public double getPrice(String product) {
        return calculatePrice(product);
    }

    private double calculatePrice(String product) {
        CommonUtils.delay();
        Random random = new Random((long) name.charAt(0) * name.charAt(1) * name.charAt(2));
        double nxtDouble = random.nextDouble();
        return nxtDouble * product.charAt(0) * product.charAt(1);
    }

    public Future<Double> getPriceAsync(String product) {
        CompletableFuture<Double> futurePrice = new CompletableFuture<>();
        new Thread(() -> {
            double price = calculatePrice(product);
            futurePrice.complete(price);
        }).start();
        return futurePrice;
    }
}
