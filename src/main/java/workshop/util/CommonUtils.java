package workshop.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;

public final class CommonUtils {

    private static final Random RANDOM = new Random(0);
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));

    private CommonUtils() {
    }

    public static void delay() {
        int delay = 500 + RANDOM.nextInt(2000);
        System.out.println("delay: " + delay + "msec");
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void delay(long delay) {
        System.out.println("delay: " + delay + "msec");
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static double format(double number) {
        synchronized (DECIMAL_FORMAT) {
            return Double.parseDouble(DECIMAL_FORMAT.format(number));
        }
    }
}
