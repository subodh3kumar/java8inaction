package workshop.util;

import workshop.model.Apple;

public interface ApplePredicate {

    boolean test(Apple apple);
}
