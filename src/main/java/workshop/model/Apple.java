package workshop.model;

import java.util.List;

public record Apple(int weight, String color) {

    public static List<Apple> getApples() {
        return List.of(
                new Apple(80, "green"),
                new Apple(155, "green"),
                new Apple(120, "red")
        );
    }
}
